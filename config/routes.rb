Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount ActionCable.server => '/cable'

  resources :conversation_rooms, only: [:index, :show, :create] do
    scope module: 'conversation_room' do
      resources :messages, only: [:index, :create]
    end
  end

  resources :users, only: :create
end
