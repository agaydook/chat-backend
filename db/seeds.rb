# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

c_1 = ConversationRoom.find_or_create_by(name: 'New conversation room')
c_2 = ConversationRoom.find_or_create_by(name: 'New conversation room 2')
c_3 =ConversationRoom.find_or_create_by(name: 'New conversation room 3')
c_4 =ConversationRoom.find_or_create_by(name: 'New conversation room 4')

Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_1, text: 'Some mesaasd sadsadsage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_1, text: 'Some masdas dsadsad sasadessage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_1, text: 'Some message asdsa das dsadsadsa sadsadsa ')

Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_2, text: 'Some mesaasd sadsadsage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_2, text: 'Some masdas dsadsad sasadessage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_2, text: 'Some measdasssage asdsa das dsadsadsa sadsadsa ')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_2, text: 'Some mesaasd sadsadsage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_2, text: 'Some masdas dsadsad sasadessage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_2, text: 'Some messaasdasdge asdsa das dsadsadsa sadsadsa ')

Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_3, text: 'Some mesaasd sadsadsage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_3, text: 'Some masdas dsadsad sasadessage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_3, text: 'Some measdasssage asdsa das dsadsadsa sadsadsa ')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_3, text: 'Some mesaasd sadsadsage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_3, text: 'Some masdas dsadsad sasadessage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_3, text: 'Some messaasdasdge asdsa das dsadsadsa sadsadsa ')

Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_4, text: 'Some mesasdfsdfsdfdsfsdasd sadsadsage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_4, text: 'Some masdas dsadsad sasadessage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_4, text: 'Some measdasssage asdsa das dsadsadsa sadsadsa ')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_4, text: 'Some mesaasd sadsadsage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_4, text: 'Some masdas dsadsad sasadessage')
Message.find_or_create_by(created_by: 'Ankain', conversation_room: c_4, text: 'Some messaasdasdge asdsa das dsadsadsa sadsadsa ')