class AddTextIndexToMessages < ActiveRecord::Migration[5.1]
  def change
    add_index :messages, :text
  end
end
