class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.references :conversation_room, foreign_key: true
      t.text :text
      t.string :created_by

      t.timestamps
    end
  end
end
