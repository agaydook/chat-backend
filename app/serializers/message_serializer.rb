class MessageSerializer < ActiveModel::Serializer
  attributes :id, :text, :created_by, :sent_at

  belongs_to :conversation_room

  def sent_at
    object.created_at.strftime('%T %F')
  end
end
