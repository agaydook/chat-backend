class ConversationRoomsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'conversation_rooms'
  end

  def unsubscribed; end
end
