class MessagesChannel < ApplicationCable::Channel
  def subscribed
    conversation_room = ConversationRoom.find(params[:conversation_id])
    stream_for conversation_room
  end

  def unsubscribed; end
end
