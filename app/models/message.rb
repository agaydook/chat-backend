class Message < ApplicationRecord
  belongs_to :conversation_room

  validates :text, length: { maximum: 2000 }
  validates_presence_of :text, :created_by
end
