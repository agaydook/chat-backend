class ConversationRoomsController < ApplicationController
  def index
    conversation_rooms = ConversationRoom.all

    render json: conversation_rooms, include: [], status: :ok
  end

  #TODO: MUST BE REFACTORED
  def create
    conversation_room = ConversationRoom.new(conversation_room_params)

    if conversation_room.save
      serialized_data = ActiveModelSerializers::Adapter::Json.new(
        ConversationRoomSerializer.new(conversation_room)
      ).serializable_hash
      ActionCable.server.broadcast 'conversation_rooms', serialized_data
      head :ok
    end
  end

  def show
    conversation_room = ConversationRoom.find(params[:id])

    render json: conversation_room, status: :ok
  end

  private

  def conversation_room_params
    params.require(:conversation_room).permit(:name)
  end
end
