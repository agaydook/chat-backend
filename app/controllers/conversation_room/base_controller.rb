class ConversationRoom::BaseController < ApplicationController
  before_action :load_conversation_room

  private

  def load_conversation_room
    @conversation_room = ConversationRoom.find(params[:conversation_room_id])
  end
end
