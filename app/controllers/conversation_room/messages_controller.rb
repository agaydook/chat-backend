class ConversationRoom::MessagesController < ConversationRoom::BaseController
  before_action :load_messages, only: :index

  def index
    render json: @messages, include: [], status: :ok
  end

  #TODO: MUST BE REFACTORED
  def create
    message = @conversation_room.messages.new(message_params)
    # @message.save!

    if message.save
      serialized_data = ActiveModelSerializers::Adapter::Json.new(
        MessageSerializer.new(message)
      ).serializable_hash
      MessagesChannel.broadcast_to @conversation_room, serialized_data
      head :ok
    end
  end

  private

  def load_messages
    @messages = @conversation_room.messages
  end

  def message_params
    params.require(:message).permit(:text, :created_by)
  end
end


